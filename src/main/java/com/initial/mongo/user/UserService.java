package com.initial.mongo.user;

import java.util.List;

public interface UserService {

    User save(User user);
    User update(User user);
    List<User> getAllUser();
    User getUser(String id);
}
