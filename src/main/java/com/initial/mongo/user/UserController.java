package com.initial.mongo.user;

import com.initial.mongo.utility.ResponseDTO;
import com.initial.mongo.utility.ResponseDetail;
import com.initial.mongo.utility.ResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    ResponseDetail responseDetail;


    @GetMapping("/list")
    public ResponseEntity<ResponseDTO> getUserList() {
        ResponseDTO responseDTO;
        List<User> users= userService.getAllUser();
        if (users != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved Users list", users);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve Users list", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/save")
    public ResponseEntity<ResponseDTO> saveUser(@RequestBody User user) {
        ResponseDTO responseDTO;
        userService.save(user);
        responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully Saved", null);
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<ResponseDTO> updateUser(@RequestBody User user) {
        ResponseDTO responseDTO;
        userService.update(user);
        responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully Saved", null);
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @GetMapping("/by/{id}")
    public ResponseEntity<ResponseDTO> getUserById(@PathVariable(name = "id") String id) {
        ResponseDTO responseDTO;
        User users= userService.getUser(id);
        if (users != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved User list", users);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve User list", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }
}
