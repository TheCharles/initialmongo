package com.initial.mongo.campaign;

import com.initial.mongo.utility.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CampaignServiceImpl implements CampaignService {

    @Autowired
    CampaignRepository campaignRepository;

    @Override
    public Campaign save(Campaign campaign) {
        return campaignRepository.save(campaign);
    }

    @Override
    public Campaign update(Campaign campaign) {
        return campaignRepository.save(campaign);
    }

    @Override
    public List<Campaign> getAllCampaign() {
        return campaignRepository.findAll();
    }

    @Override
    public Campaign findByCampaignNameAndStatus(String campaignName, Status status) {
        return campaignRepository.findByCampaignNameAndStatus(campaignName, status);
    }

    @Override
    public Page<Campaign> findByStatus(Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page-1),5, Sort.by("createdDate").descending());
        return campaignRepository.findByStatus(Status.ACTIVE,pageable);
    }

    @Override
    public Campaign getCampaign(String id) {
        return (Campaign) campaignRepository.findById(id).get();
    }
}
