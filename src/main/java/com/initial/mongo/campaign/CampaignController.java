package com.initial.mongo.campaign;

import com.initial.mongo.user.User;
import com.initial.mongo.user.UserService;
import com.initial.mongo.utility.ResponseDTO;
import com.initial.mongo.utility.ResponseDetail;
import com.initial.mongo.utility.ResponseStatus;
import com.initial.mongo.utility.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("campaign")
public class CampaignController {

    @Autowired
    ResponseDetail responseDetail;

    @Autowired
    CampaignService campaignService;

    @Autowired
    CampaignValidation campaignValidation;

    @Autowired
    UserService userService;

    @PostMapping("/save")
    public ResponseEntity<ResponseDTO> saveCampaign(@RequestBody Campaign campaign) {
        ResponseDTO responseDTO;
        CampaignError campaignError = campaignValidation.campaignError(campaign);
        User user = userService.getUser(campaign.getUserId());
        campaign.setUser(user);
        if (campaignError.getValid()) {
            campaignService.save(campaign);
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully Saved", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.VALIDATION_FAILED, "Unable to save", campaignError);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/list/{page}")
    public ResponseEntity<ResponseDTO> getCampaignList(@PathVariable("page") Long page) {
        ResponseDTO responseDTO;
        Page<Campaign> campaigns = campaignService.findByStatus(page);
        if (campaigns != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved Campaign's list", campaigns);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve Campaign's list", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update")
    public ResponseEntity<ResponseDTO> updateCampaign(@RequestBody Campaign campaign) {
        ResponseDTO responseDTO;
        User user = userService.getUser(campaign.getUserId());
        campaign.setUser(user);
        campaignService.update(campaign);
        responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully Saved", null);
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<ResponseDTO> deleteCampaign(@PathVariable(name = "id") String id) {
        ResponseDTO responseDTO;
        Campaign campaign = campaignService.getCampaign(id);
        campaign.setStatus(Status.INACTIVE);
        campaignService.update(campaign);
        responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully Deleted", null);
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @GetMapping("/by/{campaignName}")
    public ResponseEntity<ResponseDTO> getCampaignName(@PathVariable(name = "campaignName") String campaignName) {
        ResponseDTO responseDTO;
        Campaign campaign = campaignService.findByCampaignNameAndStatus(campaignName, Status.ACTIVE);
        if (campaign != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved Campaign list", campaign);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve Campaign list", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }

}
