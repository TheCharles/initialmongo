package com.initial.mongo.campaign;

import com.initial.mongo.utility.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CampaignValidation {

    @Autowired
    CampaignService campaignService;

    public CampaignError campaignError(Campaign campaign) {
        CampaignError campaignError = new CampaignError();
        Boolean valid = true;
        if (campaign.getCampaignName() == null ||
                campaign.getCampaignName().equals("") ||
                campaign.getCampaignName().replaceAll(" ", "").equals("") ||
                campaign.getCampaignName().trim().equals("")) {
            valid = false;
            campaignError.setCampaignName("Invalid campaign name!");
        }
        if (campaign.getCampaignStatus() == null ||
                campaign.getCampaignStatus().equals("")) {
            valid = false;
            campaignError.setCampaignName("Invalid campaign status!");
        }
        if (campaignService.findByCampaignNameAndStatus(campaign.getCampaignName(), Status.ACTIVE) != null) {
            valid = false;
            campaignError.setCampaignName("Campaign already exists!");
        }
        campaignError.setValid(valid);
        return campaignError;
    }
}
