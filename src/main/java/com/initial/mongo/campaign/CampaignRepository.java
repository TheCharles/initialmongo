package com.initial.mongo.campaign;

import com.initial.mongo.utility.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CampaignRepository extends MongoRepository<Campaign,String>{

    Campaign findByCampaignNameAndStatus(String campaignName, Status status);

    Page<Campaign> findByStatus(Status status, Pageable pageable);

    Optional findById(String id);

}
