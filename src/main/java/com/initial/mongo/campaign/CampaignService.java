package com.initial.mongo.campaign;

import com.initial.mongo.user.User;
import com.initial.mongo.utility.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CampaignService {

    Campaign save(Campaign campaign);

    Campaign update(Campaign campaign);

    List<Campaign> getAllCampaign();

    Campaign findByCampaignNameAndStatus(String campaignName, Status status);

    Page<Campaign> findByStatus(Long size);

    Campaign getCampaign(String id);


}
