package com.initial.mongo.lead;

import com.initial.mongo.campaign.Campaign;
import com.initial.mongo.utility.Status;
import org.springframework.data.domain.Page;

import java.util.List;

public interface LeadService {

    Lead save(Lead lead);

    Lead update(Lead lead);

    List<Lead> getAllLead();

    Page<Lead> findByStatus(Long page);

    Lead getLead(String id);
}
