package com.initial.mongo.lead;

import com.initial.mongo.campaign.Campaign;
import com.initial.mongo.campaign.CampaignService;
import com.initial.mongo.utility.ResponseDTO;
import com.initial.mongo.utility.ResponseDetail;
import com.initial.mongo.utility.ResponseStatus;
import com.initial.mongo.utility.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("lead")
public class LeadController {

    @Autowired
    ResponseDetail responseDetail;

    @Autowired
    LeadService leadService;

    @Autowired
    LeadValidation leadValidation;

//    @Autowired
//    UserService userService;

    @Autowired
    CampaignService campaignService;

    @PostMapping("/save")
    public ResponseEntity<ResponseDTO> saveLead(@RequestBody Lead lead) {
        ResponseDTO responseDTO;
        LeadError leadError = leadValidation.leadError(lead);
//        User user = userService.getUser(lead.getUserId());
        Campaign campaign = campaignService.getCampaign(lead.getCampaignId());
        lead.setCampaign(campaign);
//        lead.setUser(user);
        if (leadError.getValid()) {
            leadService.save(lead);
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully Saved", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.VALIDATION_FAILED, "Unable to Save", leadError);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/list/{page}")
    public ResponseEntity<ResponseDTO> getLeadList(@PathVariable("page") Long page) {
        ResponseDTO responseDTO;
        Page<Lead> leads = leadService.findByStatus(page);
        if (leads != null) {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully retrieved Lead list", leads);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.BAD_REQUEST, "Unable to retrieve Lead list", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<ResponseDTO> deleteLead(@PathVariable(name = "id") String id) {
        ResponseDTO responseDTO;
        Lead lead = leadService.getLead(id);
        lead.setStatus(Status.INACTIVE);
        leadService.update(lead);
        responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully Deleted", null);
        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<ResponseDTO> updateCampaign(@RequestBody Lead lead) {
        ResponseDTO responseDTO;
        LeadError leadError = leadValidation.leadError(lead);
        if (leadError.getValid()) {
            Campaign campaign = campaignService.getCampaign(lead.getCampaignId());
            lead.setCampaign(campaign);
            leadService.update(lead);
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.SUCCESS, "Successfully updated", null);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        } else {
            responseDTO = responseDetail.getResponseDTO(ResponseStatus.VALIDATION_FAILED, "Unable to update", leadError);
            return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }
}
