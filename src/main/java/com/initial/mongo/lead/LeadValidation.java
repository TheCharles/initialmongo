package com.initial.mongo.lead;

import org.springframework.stereotype.Component;

@Component
public class LeadValidation {

    public LeadError leadError(Lead lead) {
        LeadError leadError = new LeadError();
        Boolean valid = true;

        if (lead.getFirstName() == null ||
                lead.getFirstName().equals("") ||
                lead.getFirstName().replaceAll(" ", "").equals("") ||
                lead.getFirstName().trim().equals("")) {
            valid = false;
            leadError.setFirstName("Invalid first name!");
        }
        if (lead.getLastName() == null ||
                lead.getLastName().equals("") ||
                lead.getLastName().replaceAll(" ", "").equals("") ||
                lead.getLastName().trim().equals("")) {
            valid = false;
            leadError.setLastName("Invalid last name!");
        }
        if (lead.getCompany() == null ||
                lead.getCompany().equals("") ||
                lead.getCompany().replaceAll(" ", "").equals("") ||
                lead.getCompany().trim().equals("")) {
            valid = false;
            leadError.setCompany("Invalid Company name!");
        }
        if (lead.getPhoneNumber() == null ||
                lead.getPhoneNumber().equals("") ||
                lead.getPhoneNumber().replaceAll(" ", "").equals("") ||
                lead.getPhoneNumber().trim().equals("")) {
            valid = false;
            leadError.setPhoneNumber("Invalid Phone number!");
        }
        if (lead.getEmail() == null ||
                lead.getEmail().equals("") ||
                lead.getEmail().replaceAll(" ", "").equals("") ||
                lead.getEmail().trim().equals("")) {
            valid = false;
            leadError.setEmail("Invalid Email!");
        }
        if (isValidEmailAddress(lead.getEmail())==false)
        {
            valid = false;
            leadError.setEmail("Invalid Email format!");
        }
        leadError.setValid(valid);
        return leadError;
    }
    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
