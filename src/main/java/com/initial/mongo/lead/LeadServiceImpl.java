package com.initial.mongo.lead;

import com.initial.mongo.campaign.Campaign;
import com.initial.mongo.utility.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LeadServiceImpl implements LeadService {

    @Autowired
    LeadRepository leadRepository;

    @Override
    public Lead save(Lead lead) {
        return leadRepository.save(lead);
    }

    @Override
    public Lead update(Lead lead) {
        return leadRepository.save(lead);
    }

    @Override
    public List<Lead> getAllLead() {
        return leadRepository.findAll();
    }

    @Override
    public Page<Lead> findByStatus(Long page) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page-1),5, Sort.by("createdDate").descending());
        return leadRepository.findByStatus(Status.ACTIVE,pageable);
    }

    @Override
    public Lead getLead(String id) {
        return (Lead) leadRepository.findById(id).get();
    }

}
