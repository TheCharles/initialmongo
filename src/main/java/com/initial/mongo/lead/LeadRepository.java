package com.initial.mongo.lead;

import com.initial.mongo.utility.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LeadRepository extends MongoRepository<Lead, String> {

    Page<Lead> findByStatus(Status status, Pageable pageable);

    Optional findById(String id);
}
